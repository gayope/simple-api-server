<?php

namespace App;

use League\Route\Strategy\JsonStrategy;
use League\Route\Http\Exception\NotFoundException;
use Psr\Http\Server\{MiddlewareInterface, RequestHandlerInterface};
use Laminas\Diactoros\Response\JsonResponse;

class OutputStrategy extends JsonStrategy
{
    public function getNotFoundDecorator(NotFoundException $exception): MiddlewareInterface
    {
        return new Middleware\SlashMiddleware($exception);
    }

	public function getThrowableHandler(): MiddlewareInterface
    {
        return new Middleware\ErrorHandlerMiddleware();
    }
}