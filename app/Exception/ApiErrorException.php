<?php

namespace App\Exception;

use Exception;
use League\Route\Http;

class ApiErrorException extends Http\Exception
{

	public $errors = [];

    public function __construct(string $message = 'Bad Request', array $errors = [], int $code = 400)
    {
    	$this->message = $message;
    	$this->code = $code;
    	$this->errors = $errors;
    }
}
