<?php

namespace App\Controllers;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Exception\ApiErrorException;
use App\Models\User;

class HomeController extends Controller {
	public function response() {

		// throw new ApiErrorException('Error bos', ['request' => 'Tidak tau']);

		return [
	        'success' 	=> true,
	        'data' 		=> [User::first()],
	    ];
	}
}