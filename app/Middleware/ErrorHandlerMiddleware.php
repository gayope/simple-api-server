<?php

namespace App\Middleware;

use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\{MiddlewareInterface, RequestHandlerInterface};
use Laminas\Diactoros\Response\JsonResponse;

class ErrorHandlerMiddleware implements MiddlewareInterface {
	public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
	{
		try {
			return $handler->handle($request);
		} catch (\Throwable $exception) {
			$data = [
				'success' => false,
				'status_code' => $exception->getCode(),
				'message' => $exception->getMessage(),
			];

			if (isset($exception->errors) && count($exception->errors) > 0) {
				$data['error'] = $exception->errors;
			}

			$response = new JsonResponse($data);
			return $response;
		}
	}
}